import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Validation from './Validation/Validation';
import Char from './Char'

class App extends Component {
  state = {
    textLenghth: 0,
    chars: [], 
    fullString: ''
  }

  calculateLength = event => {
    const newLength = event.target.value.length
    this.setState({
      fullString: event.target.value,
      textLenghth: newLength
    })
  }

  splitTheString = event => {
    const currentChars = event.target.value.split('')
    this.setState({
      chars: currentChars
    })
  }

  deleteChar = (event, index) => {
    const newChars = [...this.state.chars]
    newChars.splice(index, 1)
    const newText = newChars.join('')
    const newLength = this.state.fullString.length - 1

    this.setState({
      chars: newChars,
      fullString: newText,
      textLenghth: newLength
    })
  }

  render() {
    const validationStyle = {
      color: 'black',
      fontSize: '20px',
    }

    let everyChar = (
      <div>
        {this.state.chars.map((cha, index) => {
          return <Char 
            letter = {cha}
            key = {index}
            delete = {event => this.deleteChar(event, index)}
          />
        })}
      </div>
    )

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <br/>
          <p className="App-title">
            Write any text you want in the next input <br/>
            (4 characters won't be enough)
          </p>
          <input type="text" onChange={this.calculateLength} onKeyUp={this.splitTheString} value={this.state.fullString}/>
          <p className="App-subtitle">Your input has {this.state.textLenghth} characters right now.</p>
        </header>
        
          {
            (this.state.textLenghth > 0) === true ? 
              <div className="App-intro">
                  <Validation 
                    style = {validationStyle}
                    length = {this.state.textLenghth}
                  />
                  <p>Now you can click on any character to delete it:</p>
              </div> : null
          }
          {everyChar}
        
      </div>
    );
  }
}

export default App;
