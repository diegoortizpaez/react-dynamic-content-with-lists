import React from 'react';

const validation = props => {

    const length = props.length
    let textOutput = null

    if (length < 5) {
        textOutput = (
            <span Style="color: red">"Text too short"</span>
        )
    }
    else{
        textOutput = (
            <span Style="color: green">"Text long enough"</span>
        )
    }
    
    return(
        <p>{textOutput}</p>
    )
}

export default validation